# README #

Requirements:

1. Works on Ubuntu
2. Requires Chef DK installed https://downloads.chef.io/chefdk
3. Requires python installed
4. Requires git installed sudo apt install git
5. git needs to be properly configured with 
* git config --global user.name
* git config --global user.email
6. ruby needs to be installed: sudo apt install ruby

### What is this repository for? ###

A micro-service (service.py) that does the following:

1. any GET request returns "Hello World!"
2. POST request with a name of a person in the body returns "Hello {name} World!"

A test script (service_test.rb) which tests the GET and POST request

A Chef deployment script which

- Creates a test directory and checks out from bitbucket service.py
- Checks that port 80 is opened.  If not, it will open the firewall port 
- kills all running instances of service.py
- runs service.py from test location
- runs automated tests /tests/service_test.py
- kills all running instances of service.py
- copies the test location to the production location
- runs the updated python scripts in production

### How do I get set up? ###

How to run tests?

start HTTP service: python ~/service/service.py
run tests: python ~/service/tests/service_test.py

automated test makes a GET and POST request to HTTP server via curl:
curl http://localhost
curl -d "body" http://localhost 

# How to Set up? #

* login as as root and go to home directory
* checkout repo and go to: cd ~/interview 
* Go to config file: /root/interview/cookbooks/deploy/attributes/default.rb and add your bitbucket credentials
* sudo chef-client --local-mode --runlist 'recipe[deploy]'

# How do I scale infrastructure? #

* For scaling the micro service, provision a new VM with Ubuntu OS, install chefdk and run the chef commands.
* Or, have the node configured under chef server 

**Optimization Note:** 
* For simplicity the script is implemented in python.  However, due to python threading constraints, rewrite in java for performance optimizations.

### Who do I talk to? ###

* Repo owner or admin

### Enhancements ###
There is a drawback to this deployment script as everytime it deploys it needs to stop all services running on port 80.  This will cause service disruption everytime you deploy. Its fine for now, however, as the services begin to grow, startup time will begin to take longer as well as running automated tests will also take longer. To fix this problem, we need a build/test server which the script can make the tests.  Then, deploy service.py to production server via scp.  This should minimize downtime.