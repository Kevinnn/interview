from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer

class serviceHandler(BaseHTTPRequestHandler):
	#For Testing: curl -I http://localhost
	def _set_headers(self):
        	self.send_response(200)
        	self.send_header('Content-type', 'text/html')
        	self.end_headers()
	
	#For Testing: curl http://localhost
	def do_GET(self):
		self._set_headers()
        	self.wfile.write("Hello World!\n")		
	# For testing: curl -d "foo=bar&bin=baz" http://localhost
	def do_POST(self):
		self._set_headers()
		post_data = self.rfile.read(int(self.headers['Content-Length']))
        	self.wfile.write("Hello " + str(post_data) + " world!\n")
	
def run(server_class=HTTPServer, handler_class=serviceHandler, port=80):
	server_address = ('localhost', 80)
    	httpd = server_class(server_address, handler_class)
    	print 'Starting httpd...'
    	httpd.serve_forever()

run()
