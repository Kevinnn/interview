#
# Cookbook:: deploy
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

def shutdown_services()
	#check if existing process with name service.py is running
	res = `pgrep -f service.py`

	if res.to_i != 0 #check if pid is an integer
        	cmd = "kill -9 " + res.to_s
        	`#{cmd}`
        	print "shut off service.py \n"
	else
        	print "service.py is not running.  Nothing to do... \n"
	end
end

#Checkout the git repo in a test location
if File.directory?("~/test")
	`rm -rf ~/test`
end

`mkdir ~/test; cd ~/test`

cmd = "git clone https://" + node["bitbucket_username"] + ":" + node["bitbucket_password"] + "@bitbucket.org/Kevinnn/interview.git"
`#{cmd}`

#install curl as automated tests are dependant on curl
print "installing curl... \n"
`apt install curl`

# For Ubuntu python is by default installed
# For other distributions, we might want to Check this.

#Open port 80 iptables
print "Opening port 80... \n"
`sudo iptables -A INPUT -p tcp --dport 80 -j ACCEPT`

#run test service
print "starting service.py in test location \n"

shutdown_services() #shutdown all services running on port 80 before running test environment
Process.fork do
	cmd = 'python ' + node['test_service'] + "&"
	`#{cmd}`
end

#Since the HTTP service.py is running forever, we cannot wait for this process
#to complete.  Therefore, use a estimate of 10 sec to ensure the service
#starts up before we start running the automated tests.
sleep(10)

#run automated tests 
#failed cases will stop this script from running
#deployment will also fail
print "running automated tests... \n"
cmd = 'ruby ' + node['test_script']
ret = `#{cmd}`

#copy service.py to production location
print "All tests passed, deploying to production \n"
`cp -rf ~/interview/service ~/`

#Shutting down test environment
shutdown_services() 

#run production service
print "starting services \n"
cmd = "python " + node["production_service"]
Process.fork do
	`#{cmd}`
end

print "done \n"

#remove test folder (optional)
`rm -rf ~/test`
