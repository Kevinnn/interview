default['test_script'] = '~/interview/service/tests/service_test.rb'

# NOTE: when filling out your bitbucket password
# you need "".  For example, if your password is
# Hello, write:
# default['bitbucket_password']="\"Hello\""
default['bitbucket_password']="\"<Enter bitbucket password here>\""
default['bitbucket_username']="<Enter bitbucket username here>"

default['test_service']="~/interview/service/service.py"
default['production_service']= "~/service/service.py"

